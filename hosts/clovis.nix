# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  hmUser,
  config,
  pkgs,
  modulesPath,
  ...
}: {
  nix.settings.experimental-features = ["nix-command" "flakes" "ca-derivations"];
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    ../profiles/common.nix

    ../modules/de
    ../profiles/workstation.nix
    ../profiles/backups

    (import ../profiles/langs.nix ["rust" "nix" "k8s" "virt" "go"])
  ];

  networking.hostName = "clovis";

  # Boot
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.availableKernelModules = ["xhci_pci" "ahci" "uas" "usbhid" "sd_mod"];
  boot.initrd.kernelModules = [];
  boot.kernelModules = ["kvm-amd"];
  boot.extraModulePackages = [];

  # TODO: switch away
  networking.networkmanager.enable = true;

  # Desktop environment
  services.ariade = {
    enable = true;
    twoMonitors = true;
    lockScreen = false;
    kbLayout = "us";
  };
  colours.scheme = {
    flavour = "latte";
    accent = "lavender";
  };

  # Filesystems
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/790316d0-1683-4069-bef7-1cee7d76481b";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/ECEC-2AA6";
    fsType = "vfat";
  };

  swapDevices = [];

  hardware.cpu.amd.updateMicrocode = config.hardware.enableRedistributableFirmware;

  # NVIDIA
  services.xserver.videoDrivers = ["nvidia"];
  hardware.nvidia = {
    modesetting.enable = true;
    open = false;
    powerManagement.enable = true;
  };
  hardware.opengl = {
    enable = true;
    driSupport = true;
    extraPackages = with pkgs; [
      nvidia-vaapi-driver
    ];
  };
  environment.variables = {
    # Necessary to correctly enable va-api (video codec hardware
    # acceleration). If this isn't set, the libvdpau backend will be
    # picked, and that one doesn't work with most things, including
    # Firefox.
    LIBVA_DRIVER_NAME = "nvidia";
    # Required to run the correct GBM backend for nvidia GPUs on wayland
    GBM_BACKEND = "nvidia-drm";
    # Apparently, without this nouveau may attempt to be used instead
    # (despite it being blacklisted)
    __GLX_VENDOR_LIBRARY_NAME = "nvidia";

    # Required to use va-api it in Firefox. See
    # https://github.com/elFarto/nvidia-vaapi-driver/issues/96
    MOZ_DISABLE_RDD_SANDBOX = "1";
    # It appears that the normal rendering mode is broken on recent
    # nvidia drivers:
    # https://github.com/elFarto/nvidia-vaapi-driver/issues/213#issuecomment-1585584038
    NVD_BACKEND = "direct";
  };
}
