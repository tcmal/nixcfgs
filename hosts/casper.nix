{
  lib,
  hmUser,
  config,
  pkgs,
  modulesPath,
  ...
}: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    ../profiles/common.nix
    ../profiles/sboot.nix

    ../modules/de
    ../profiles/workstation.nix

    (import ../profiles/langs.nix ["nix" "rust" "go" "k8s"])
  ];

  networking.hostName = "casper";

  # Authentication done by disk decryption
  services.getty.autologinUser = hmUser;

  # Boot
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.availableKernelModules = ["nvme" "xhci_pci" "uas" "sd_mod" "rtsx_pci_sdmmc"];
  boot.initrd.kernelModules = [];
  boot.kernelModules = ["kvm-amd"];
  boot.extraModulePackages = [];

  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  # Desktop environment
  services.ariade = {
    enable = true;
    hasBattery = true;
    kbLayout = "gb";
  };
  colours.scheme = {
    flavour = "latte";
    accent = "sky";
  };

  # Filesystems
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/9c87ba9f-19f5-494d-9cbf-366508b6d474";
    fsType = "ext4";
  };

  boot.initrd.luks.devices."root".device = "/dev/disk/by-uuid/3725d1d8-f4b3-454f-a519-7f14f73a0917";

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/509F-2EDE";
    fsType = "vfat";
  };

  swapDevices = [];
}
