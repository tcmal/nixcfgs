{
  pkgs,
  config,
  lib,
  ...
}:
with lib; let
  inherit (strings) escapeShellArg;
  moz-idb-edit = pkgs.callPackage ../../pkgs/moz-idb-edit {};
  json = pkgs.formats.json {};
  cfg = config.programs.firefox.storagePatches;
  genSetter = patchDesc: with patchDesc; "mozidbedit --extension ${escapeShellArg extension} -S ${escapeShellArg storageDb} ${escapeShellArg storageKey} ${escapeShellArg glomPath} --set-type json --set ${escapeShellArg (builtins.toJSON value)};\n";
  setterScript = ''
    #!/usr/bin/env sh
    export PATH=${moz-idb-edit}/bin:$PATH
    ${concatStrings (map genSetter cfg)}
  '';
in {
  # TODO: Technically this should be per-profile
  options.programs.firefox.storagePatches = mkOption {
    type = with types;
      listOf (submodule {
        options = {
          extension = mkOption {type = str;};
          storageDb = mkOption {
            type = str;
            default = "webExtensions-storage-local";
          };
          storageKey = mkOption {type = str;};
          glomPath = mkOption {
            type = str;
            default = "@";
          };
          value = mkOption {type = json.type;};
        };
      });
    description = "Set of attributes to apply. In format {extensionId = {storageKey = {glomPath = jsonVal}}}";
  };

  config = mkIf config.programs.firefox.enable {
    home.file.".local/bin/firefox-extsetup" = {
      text = setterScript;
      executable = true;
      onChange = "$HOME/.local/bin/firefox-extsetup";
    };
    home.sessionPath = ["$HOME/.local/bin"];
  };
}
