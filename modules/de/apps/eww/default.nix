{
  hmUser,
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.services.ariade;
in
  mkIf cfg.enable {
    home-manager.users.${hmUser}.programs.eww = {
      enable = true;
      configDir = let
        args = {
          inherit cfg;
          inherit (config) colours;
        };
        scss = import ./eww.scss.nix args;
        yuck = import ./eww.yuck.nix args;
      in
        pkgs.runCommand "eww-config" {} ''
          mkdir -p $out
          echo ${lib.strings.escapeShellArg scss} > $out/eww.scss
          echo ${lib.strings.escapeShellArg yuck} > $out/eww.yuck
        '';
    };
  }
