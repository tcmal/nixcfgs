{
  cfg,
  colours,
}:
with colours.values; ''

  $bg: ${base.hex};
  $bg-lighter: ${crust.hex};
  $fg: ${text.hex};
  $red: ${red.hex};
  $accent: ${accent.hex};

  * { all: unset; }

  .layout-box {
      font-family: 'Symbols Nerd Font Mono', sans-serif;
      padding: 5em;
      color: $fg;

      background: rgba(0, 0, 0, 0.8);
      animation: fade 200ms;
  }

  @keyframes fade {
      from {
          opacity: 0;
      }
      to {
          opacity: 1;
      }
  }

  .stat-box {
      background: $bg-lighter;
      :first-child {
          background: $bg;
      }
      label {
          font-family: monospace;
          font-size: 1.5em;
          padding: 0.75rem;
      }
  }

  .close-btn {
      font-size: 1.5em;
      &:hover {
          color: $red;
      }
  }

  .revealer-icon {
      font-size: 1.5em;
      padding: 0.75rem;
      background: $bg;
  }

  .revealed {
      padding-left: 1rem;
  }

  .btns-box {
      font-size: 1.5em;

      button {
          font-family: 'Symbols Nerd Font Mono';
          padding: 0.75em;
          background-color: $bg;

          &:hover {
              transition: 200ms linear background-color, border-radius;
              background-color: $bg-lighter;
          }

          &:first-child {
              color: $red;
          }
      }
  }


  scale trough {
      all: unset;
      border-radius: 5px;
      min-height: 10px;
      min-width: 80px;
      margin: .3rem 0 .3rem 0;
  }


  .volbar trough highlight {
      background-color: $accent;
      border-radius: 5px;
  }
''
