{
  colours,
  cfg,
}: ''
  (defwindow overlay
      :stacking "fg"
      :windowtype "desktop"
      :wm-ignore true
      :geometry (geometry :width "100%" :height "100%")
      (overlay_layout))

  (defpoll time :interval "5s"
           :initial "{\"hour\": 0, \"min\": 0}"
           `date +'{"hour":"%H","min":"%M"}'`)

  (defpoll date :interval "1h"
           :initial "{\"weekday\": \"\", \"day\": 0, \"month\": 0}"
           `date +'{"weekday": "%a", "day":"%d","month":"%m"}'`)

  (defpoll volume :interval "5s"
           :initial 0.0
           `wpctl get-volume @DEFAULT_SINK@ | cut -d ' ' -f 2 | sed 's/\\.//'`)

  (defwidget overlay_layout []
    (box :class "layout-box" :space-evenly false :orientation "vertical"
         (box :valign "start" :space-evenly false :spacing 25
              ${
    if cfg.hasBattery
    then "(_battery)"
    else ""
  }
              (_volume)
              (box :hexpand true :halign "end"
                   (button :onclick "eww close overlay" :class "close-btn" "󰅖")))
         (box :vexpand true)
         (box :space-evenly false :hexpand true :vexpand false :spacing 10
              (_stat :icon "󰸘" :value "''${date.weekday} ''${date.day}/''${date.month}")
              (_stat :icon "󰥔" :value "''${time.hour}:''${time.min}")
              (_stat :icon "" :value "''${round(EWW_CPU.avg, 2)}")
              (_stat :icon "󰍛" :value "''${round(EWW_RAM.used_mem_perc, 2)}%"))))


  (defwidget _battery []
    (_stat :icon "''${EWW_BATTERY.BAT0.status == 'Charging' ? '' : ''}"
           :value "''${EWW_BATTERY.BAT0.capacity }%"))

  (defwidget _volume []
    (box :vexpand true :space-evenly false
         :class "revealer-icon"
         (label :text "")
         (scale :class "revealed volbar"
                :value volume
                :orientation "h"
                :max 100
                :min 0
                :onchange "wpctl set-volume @DEFAULT_SINK@ {}%" )))

  (defwidget _stat [icon value]
             (box :class "stat-box" :spacing 5 :vexpand true :space-evenly false
                  (label :text icon)
                  (label :text value)))

''
