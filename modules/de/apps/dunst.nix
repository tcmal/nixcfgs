{
  pkgs,
  config,
  hmUser,
  lib,
  ...
}: let
  cfg = config.services.ariade;
  schemes = {
    latte = pkgs.fetchurl {
      url = "https://raw.githubusercontent.com/catppuccin/dunst/main/src/latte.conf";
      hash = "";
    };
    macchiato = pkgs.fetchurl {
      url = "https://raw.githubusercontent.com/catppuccin/dunst/main/src/macchiato.conf";
      hash = "";
    };
    frappe = pkgs.fetchurl {
      url = "https://raw.githubusercontent.com/catppuccin/dunst/main/src/frappe.conf";
      hash = "sha256-bXBPm5/lApJpztE3aAwT7ggSMEvUpZqVLZiVQzPONyQ=";
    };
    mocha = pkgs.fetchurl {
      url = "https://raw.githubusercontent.com/catppuccin/dunst/main/src/mocha.conf";
      hash = "";
    };
  };
in
  lib.mkIf (cfg.enable) {
    home-manager.users.${hmUser}.services.dunst = with config.colours.values; {
      enable = true;
      configFile = pkgs.writeText "dunstrc" ''
        [global]
        follow = keyboard
        font = "Sans 16"

        frame_color = "${accent.hex}"
        separator_color= frame

        [urgency_low]
        background = "${base.hex}"
        foreground = "${text.hex}"

        [urgency_normal]
        background = "${base.hex}"
        foreground = "${text.hex}"

        [urgency_critical]
        background = "${text.hex}"
        foreground = "${base.hex}"
        frame_color = "${peach.hex}"
      '';
    };
  }
