{
  pkgs,
  config,
  hmUser,
  lib,
  ...
}: let
  cfg = config.services.ariade;
  schemes = {
    latte = pkgs.fetchurl {
      url = "https://github.com/catppuccin/alacritty/raw/yaml/catppuccin-latte.yml";
      hash = "sha256-+K4ZBLSY/p+av+kTTcYKeVfd8BuDExXDNij7hYYTlkc=";
    };
    macchiato = pkgs.fetchurl {
      url = "https://github.com/catppuccin/alacritty/raw/yaml/catppuccin-macchiato.yml";
      hash = "sha256-+m8FyPStdh1A1xMVBOkHpfcaFPcyVL99tIxHuDZ2zXI=";
    };
    frappe = pkgs.fetchurl {
      url = "https://github.com/catppuccin/alacritty/raw/yaml/catppuccin-frappe.yml";
      hash = "sha256-TywgT/Gf0TjcuHGNzgAMNvueCo23BEL2b9cGJaT2vkw=";
    };
    mocha = pkgs.fetchurl {
      url = "https://github.com/catppuccin/alacritty/raw/yaml/catppuccin-mocha.yml";
      hash = "sha256-28Tvtf8A/rx40J9PKXH6NL3h/OKfn3TQT1K9G8iWCkM=";
    };
  };
in
  lib.mkIf (cfg.enable) {
    home-manager.users.${hmUser}.programs.alacritty = {
      enable = true;
      settings = {
        import = [
          "${schemes.${config.colours.scheme.flavour}}"
        ];
        colors.draw_bold_text_with_bright_colors = false;
        env.TERM = "xterm-256color";
        font.size = 12;
        window.padding = {
          x = 10;
          y = 10;
        };
      };
    };
  }
