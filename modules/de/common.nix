{
  hmUser,
  pkgs,
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.services.ariade;
in
  mkIf cfg.enable {
    # Font stack
    fonts = {
      packages = with pkgs; [
        liberation_ttf
        inter
        monaspace

        (nerdfonts.override {fonts = ["NerdFontsSymbolsOnly"];}) # Icons
        noto-fonts-color-emoji # Emoji
        dejavu_fonts # Fallback
      ];
      fontconfig.defaultFonts = let
        serif = "Liberation Serif";
        sansSerif = "Inter";
        monospace = "Monaspace Neon";
        symbol = "Symbols Nerd Font Mono";
        emoji = "Noto Color Emoji";
        fallbacks = [symbol emoji "DejaVu Sans Mono"];
      in {
        serif = [serif] ++ fallbacks;
        sansSerif = [sansSerif] ++ fallbacks;
        monospace = [monospace] ++ fallbacks;
        emoji = [emoji] ++ fallbacks;
      };
    };

    # Audio
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
    };

    # Networking
    systemd.network.wait-online.enable = false;
    boot.initrd.systemd.network.wait-online.enable = false;
    # Don't wait for network startup
    # https://old.reddit.com/r/NixOS/comments/vdz86j/how_to_remove_boot_dependency_on_network_for_a
    systemd = {
      targets.network-online.wantedBy = pkgs.lib.mkForce []; # Normally ["multi-user.target"]
      services.NetworkManager-wait-online.wantedBy = pkgs.lib.mkForce []; # Normally ["network-online.target"]
    };
    networking.firewall.logRefusedConnections = false;
    networking.networkmanager = {
      enable = true;
      plugins = lib.mkForce [];
    };

    # Natural scrolling
    services.xserver.libinput.touchpad.naturalScrolling = true;

    # Needed for GTK configuration & some apps
    programs.dconf.enable = true;

    # Lock on suspend
    programs.i3lock = {
      enable = true;
    };

    systemd.services.lock-suspend = mkIf cfg.lockScreen {
      before = ["sleep.target"];
      wantedBy = ["sleep.target"];
      environment.DISPLAY = ":0";
      serviceConfig.User = hmUser;
      script = "/run/current-system/sw/bin/i3lock -n --image ${./wallpaper.png}";
    };

    programs.light.enable = true;

    home-manager.users.${hmUser} = {
      # GTK
      gtk = {
        enable = true;
        theme = let
          inherit (config.colours.scheme) flavour accent;
          capFirst = str: (lib.toUpper (lib.substring 0 1 str) + lib.substring 1 (-1) str);
        in {
          name = "Catppuccin-${capFirst flavour}-Standard-${capFirst accent}-${
            if flavour == "latte"
            then "Light"
            else "Dark"
          }";
          package = pkgs.catppuccin-gtk.override {
            variant = flavour;
            accents = [accent];
            tweaks = ["rimless"];
            size = "standard";
          };
        };
        font = {
          name = builtins.head config.fonts.fontconfig.defaultFonts.sansSerif;
          size = 12;
        };
      };

      # Battery notifications
      services.batsignal = mkIf cfg.hasBattery {
        enable = true;
        extraArgs = ["-c" "10" "-w" "20"];
      };
    };
  }
