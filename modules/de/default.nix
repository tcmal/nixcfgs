{
  lib,
  config,
  pkgs,
  ...
}:
with lib; {
  options.services.ariade = {
    enable = mkEnableOption "Aria's Desktop Environment";
    lockScreen = mkOption {
      type = types.bool;
      default = true;
    };
    twoMonitors = mkOption {
      type = types.bool;
      default = false;
    };
    hasBattery = mkOption {
      type = types.bool;
      default = false;
    };
    kbLayout = mkOption {
      type = types.str; # TODO
      default = "gb";
    };
    startupCmds = mkOption {
      type = types.lines;
      default = "";
    };
    autostart = mkOption {
      type = types.bool;
      default = true;
    };
  };

  imports = [
    ./common.nix
    ./scheme.nix
    ./dwm
    ./apps/alacritty.nix
    ./apps/eww
    ./apps/dunst.nix
  ];
}
