{
  pkgs,
  lib,
  kbLayout,
  colours,
  ...
}: ''
  #include <X11/XF86keysym.h>

  // appearance
  static const unsigned int borderpx  = 1;        /* border pixel of windows */
  static const unsigned int snap      = 32;       /* snap pixel */
  static const int showbar            = 0;        /* 0 means no bar */
  static const int topbar             = 1;        /* 0 means bottom bar */
  static const char *fonts[]          = { "monospace:size=10" };
  static const char dmenufont[]       = "monospace:size=10";
  static const char *colors[][3]      = {
  	/*               fg         bg         border   */
  	[SchemeNorm] = { "${colours.text.hex}", "${colours.base.hex}", "${colours.base.hex}" },
  	[SchemeSel]  = { "${colours.lavender.hex}", "${colours.lavender.hex}", "${colours.lavender.hex}" },
  };

  // tagging
  static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

  // rules
  static const Rule rules[] = {
    /* class      instance    title       tags mask     isfloating   monitor */
    { NULL,     NULL,       NULL,       0,            0,           -1 },
  };

  // layouts
  static const float mfact     = 0.6; /* factor of master area size [0.05..0.95] */
  static const int nmaster     = 1;    /* number of clients in master area */
  static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
  static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */
  static const Layout layouts[] = {
  	/* symbol     arrange function */
  	{ "TTT",      bstack },
  	{ "[]=",      tile },
  	{ "[M]",      monocle },
  	{ "><>",      NULL },    /* no layout function means floating behavior */
  };


  #define MODKEY Mod4Mask

  #define TAGKEYS(KEY,TAG) \
  	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
  	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
  	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
  	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

  /* helper for spawning shell commands in the pre dwm-5.0 fashion */
  #define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

  // commands
  static const char *termcmd[] = { "alacritty", NULL };
  static const char *menucmd[] = { "j4-dmenu-desktop", NULL };
  static const char *upvol[]   = { "/run/current-system/sw/bin/wpctl",   "set-volume", "@DEFAULT_AUDIO_SINK@",      "5%+",      NULL };
  static const char *downvol[] = { "/run/current-system/sw/bin/wpctl",   "set-volume", "@DEFAULT_AUDIO_SINK@",      "5%-",      NULL };
  static const char *mutevol[] = { "/run/current-system/sw/bin/wpctl",   "set-mute",   "@DEFAULT_AUDIO_SINK@",      "toggle",   NULL };
  static const char *upbright[] = { "/run/current-system/sw/bin/light",  "-A", "5", NULL };
  static const char *downbright[] = { "/run/current-system/sw/bin/light","-U", "5", NULL };
  static const char *infocmd[] = { "sh", "-c", "if pgrep eww; then pkill eww; else eww daemon && eww open overlay; fi", NULL };
  static const char *lockcmd[] = { "/run/current-system/sw/bin/i3lock", "-n", "--image", "${../wallpaper.png}", NULL };
  static const char *screenshotcmd[] = { "sh", "-c", "screenshot", NULL };

  // keybinds
  static const Key keys[] = {
  	/* modifier                  key                 function        argument */
    // launchers
  	{ MODKEY,                    XK_Return,               spawn, {.v = menucmd} },
  	{ MODKEY,                    XK_t,                    spawn, {.v = termcmd} },
  	{ MODKEY,                    XK_i,                    spawn, {.v = infocmd} },
  	{ MODKEY,                    XK_l,                    spawn, {.v = lockcmd} },
  	{ 0,                         XF86XK_AudioLowerVolume, spawn, {.v = downvol } },
  	{ 0,                         XK_F7,                   spawn, {.v = downvol } },
  	{ 0,                         XF86XK_AudioMute,        spawn, {.v = mutevol } },
  	{ 0,                         XK_F6,                   spawn, {.v = mutevol } },
  	{ 0,                         XF86XK_AudioRaiseVolume, spawn, {.v = upvol   } },
  	{ 0,                         XK_F8,                   spawn, {.v = upvol   } },
  	{ 0,                         XF86XK_MonBrightnessUp,  spawn, {.v = upbright } },
  	{ 0,                         XF86XK_MonBrightnessDown,spawn, {.v = downbright } },
  	{ 0,                         XK_Print                ,spawn, {.v = screenshotcmd } },

    // navigation
  	{ MODKEY,                    XK_j,          focusstack,     {.i = +1} },
  	{ MODKEY,                    XK_k,          focusstack,     {.i = -1} },
  	{ MODKEY|ShiftMask,          XK_q,          killclient,     {0} },

    // layout
  	{ MODKEY,                    XK_f,          zoom,            {0} },
  	{ MODKEY,                    XK_v,          setlayout,       {.v = &layouts[0]} }, // bottom stack
  	{ MODKEY,                    XK_h,          setlayout,       {.v = &layouts[1]} }, // tile
  	{ MODKEY,                    XK_m,          setlayout,       {.v = &layouts[2]} }, // monocle
  	{ MODKEY,                    XK_a,          setlayout,       {.v = &layouts[3]} }, // floating

  	{ MODKEY|ShiftMask,          XK_space,      togglefloating,  {0} },

  	{ MODKEY,                    XK_bracketleft,  incnmaster,      {.i = +1} },
  	{ MODKEY,                    XK_bracketright, incnmaster,      {.i = -1} },
  	{ MODKEY,                    XK_o,            setmfact,        {.f = -0.05} },
  	{ MODKEY,                    XK_p,            setmfact,        {.f = +0.05} },

    // monitors
  	{ MODKEY,                    XK_w,          focusmon,       {.i = -1} },
  	{ MODKEY,                    XK_e,          focusmon,       {.i = 1} },

    // workspaces
  	{ MODKEY,                    XK_Tab,        view,           {0} }, // go to prev
  	{ MODKEY,                    XK_0,          view,           {.ui = ~0} }, // view all
    TAGKEYS(                     XK_1,                      0)
    TAGKEYS(                     XK_2,                      1)
    TAGKEYS(                     XK_3,                      2)
    TAGKEYS(                     XK_4,                      3)
    TAGKEYS(                     XK_5,                      4)
    TAGKEYS(                     XK_6,                      5)
    TAGKEYS(                     XK_7,                      6)
    TAGKEYS(                     XK_8,                      7)
    TAGKEYS(                     XK_9,                      8)

  	{ MODKEY|ShiftMask,          XK_Escape,     quit,       {0} },
  };

  // mouse
  static const Button buttons[] = {
     { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
     { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
     { ClkWinTitle,          0,              Button2,        zoom,           {0} },
     { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
     { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
     { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
     { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
     { ClkTagBar,            0,              Button1,        view,           {0} },
     { ClkTagBar,            0,              Button3,        toggleview,     {0} },
     { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
     { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
  };
''
