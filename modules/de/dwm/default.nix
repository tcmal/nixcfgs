{
  hmUser,
  pkgs,
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.services.ariade;
  patchedDwm =
    (pkgs.dwm.override {
      conf = import ./config.h.nix (cfg
        // {
          inherit pkgs lib;
          colours = config.colours.values;
        });
    })
    .overrideAttrs {
      patches = [
        # All of these patches are adapted from patches on the dwm wiki: https://dwm.suckless.org/patches/
        # singletagset - Allows for a single set of tags to be shared between multiple monitors
        #   https://dwm.suckless.org/patches/single_tagset/
        ./0001-singletagset.patch
        # cursorwarp - When focus is changed, warp the cursor the the center of the new focused client
        #   https://dwm.suckless.org/patches/cursorwarp/
        ./0002-cursorwarp.patch
        # autostart - Allow dwl to execute commands from autostart array in your config.h file
        #   https://dwm.suckless.org/patches/autostart/
        ./0003-autostart.patch
        # smartborders - The borders of a window aren't drawn when is the only tiling window in its tag or is in a monocle layout
        #   https://dwm.suckless.org/patches/smartborders/
        ./0004-smartborders.patch
        # Bottomstack - bstack and bstackhoriz
        #   https://dwm.suckless.org/patches/bottomstack/
        ./0005-bottomstack.patch
      ];
    };
  dmenu-desktop = pkgs.j4-dmenu-desktop;
in
  mkIf (cfg.enable) {
    # Install packages
    environment.systemPackages = with pkgs; [
      patchedDwm

      alacritty # Terminal
      dmenu # Launcher
      dmenu-desktop
      gnome.adwaita-icon-theme # Icons / cursors

      pavucontrol # Audio
      feh # Wallpaper
      xclip

      # Screenshots
      pkgs.maim
      pkgs.xclip
      (pkgs.writeShellScriptBin "screenshot" ''
        maim -s | xclip -selection clipboard -t image/png
      '')
    ];

    # X11 setup
    services.xserver = {
      enable = true;
      displayManager.startx.enable = true;
    };
    hardware.opengl = {
      enable = true;
      driSupport = true;
    };

    nixpkgs.overlays = [
      # Set dmenu colour scheme
      (final: prev:
        prev
        // {
          dmenu = prev.dmenu.overrideAttrs (_: {
            patches = [(pkgs.writeText "config.h.patch" (import ./dmenu-config.h.nix config.colours.values))];
          });
        })
    ];

    # Compositor
    services.picom = {
      enable = true;
      vSync = true;
      backend = "glx";
    };

    home-manager.users.${hmUser} = {
      # Autostart on login
      home.file.".xinitrc".text = ''
        if test -z "$DBUS_SESSION_BUS_ADDRESS"; then
            eval $(dbus-launch --exit-with-session --sh-syntax)
        fi
        systemctl --user import-environment DISPLAY XAUTHORITY XDG_SESSION_ID

        if command -v dbus-update-activation-environment >/dev/null 2>&1; then
            dbus-update-activation-environment DISPLAY XAUTHORITY
        fi

        setxkbmap -option ctrl:swapcaps ${cfg.kbLayout}
        exec dwm
      '';
      programs.bash = mkIf cfg.autostart {
        profileExtra = "[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx";
      };
      programs.fish = mkIf cfg.autostart {
        loginShellInit = ''
          if status is-login
              if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
                  exec startx -- -keeptty
              end
          end
        '';
      };

      # systemd stuff
      systemd.user.targets.dwm-session.Unit = {
        BindsTo = "graphical-session.target";
        Wants = "graphical-session-pre.target";
        After = "graphical-session-pre.target";
      };

      home.file.".dwm/autostart.sh" = {
        executable = true;
        text = ''
          #!/usr/bin/env sh
          systemctl --user start dwm-session.target
          ${pkgs.feh}/bin/feh --no-xinerama --bg-scale ${
            if cfg.twoMonitors
            then ../wallpaper_2monitors.png
            else ../wallpaper.png
          }

          ${cfg.startupCmds}
        '';
      };
    };
  }
