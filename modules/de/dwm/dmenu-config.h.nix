colours: ''
  diff -up a/config.def.h b/config.def.h
  --- a/config.def.h	2022-10-04 18:36:58.000000000 +0100
  +++ b/config.def.h	2024-01-21 16:45:09.776646609 +0000
  @@ -9,9 +9,9 @@ static const char *fonts[] = {
   static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
   static const char *colors[SchemeLast][2] = {
   	/*     fg         bg       */
  -	[SchemeNorm] = { "#bbbbbb", "#222222" },
  -	[SchemeSel] = { "#eeeeee", "#005577" },
  -	[SchemeOut] = { "#000000", "#00ffff" },
  +	[SchemeNorm] = { "${colours.text.hex}", "${colours.base.hex}" },
  +	[SchemeSel] = { "${colours.base.hex}", "${colours.accent.hex}" },
  +	[SchemeOut] = { "#000000", "${colours.accent.hex}" },
   };
   /* -l option; if nonzero, dmenu uses vertical list with given number of lines */
   static unsigned int lines      = 0;
''
