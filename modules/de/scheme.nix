{
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.colours;
  schemes = {
    latte = {
      rosewater = {
        hex = "#dc8a78";
        r = 220;
        g = 138;
        b = 120;
      };
      flamingo = {
        hex = "#dd7878";
        r = 221;
        g = 120;
        b = 120;
      };
      pink = {
        hex = "#ea76cb";
        r = 234;
        g = 118;
        b = 203;
      };
      mauve = {
        hex = "#8839ef";
        r = 136;
        g = 57;
        b = 239;
      };
      red = {
        hex = "#d20f39";
        r = 210;
        g = 15;
        b = 57;
      };
      maroon = {
        hex = "#e64553";
        r = 230;
        g = 69;
        b = 83;
      };
      peach = {
        hex = "#fe640b";
        r = 254;
        g = 100;
        b = 11;
      };
      yellow = {
        hex = "#df8e1d";
        r = 223;
        g = 142;
        b = 29;
      };
      green = {
        hex = "#40a02b";
        r = 64;
        g = 160;
        b = 43;
      };
      teal = {
        hex = "#179299";
        r = 23;
        g = 146;
        b = 153;
      };
      sky = {
        hex = "#04a5e5";
        r = 4;
        g = 165;
        b = 229;
      };
      sapphire = {
        hex = "#209fb5";
        r = 32;
        g = 159;
        b = 181;
      };
      blue = {
        hex = "#1e66f5";
        r = 30;
        g = 102;
        b = 245;
      };
      lavender = {
        hex = "#7287fd";
        r = 114;
        g = 135;
        b = 253;
      };
      text = {
        hex = "#4c4f69";
        r = 76;
        g = 79;
        b = 105;
      };
      subtext1 = {
        hex = "#5c5f77";
        r = 92;
        g = 95;
        b = 119;
      };
      subtext0 = {
        hex = "#6c6f85";
        r = 108;
        g = 111;
        b = 133;
      };
      overlay2 = {
        hex = "#7c7f93";
        r = 124;
        g = 127;
        b = 147;
      };
      overlay1 = {
        hex = "#8c8fa1";
        r = 140;
        g = 143;
        b = 161;
      };
      overlay0 = {
        hex = "#9ca0b0";
        r = 156;
        g = 160;
        b = 176;
      };
      surface2 = {
        hex = "#acb0be";
        r = 172;
        g = 176;
        b = 190;
      };
      surface1 = {
        hex = "#bcc0cc";
        r = 188;
        g = 192;
        b = 204;
      };
      surface0 = {
        hex = "#ccd0da";
        r = 204;
        g = 208;
        b = 218;
      };
      base = {
        hex = "#eff1f5";
        r = 239;
        g = 241;
        b = 245;
      };
      mantle = {
        hex = "#e6e9ef";
        r = 230;
        g = 233;
        b = 239;
      };
      crust = {
        hex = "#dce0e8";
        r = 220;
        g = 224;
        b = 232;
      };
    };
    frappe = {
      rosewater = {
        hex = "#f2d5cf";
        r = 242;
        g = 213;
        b = 207;
      };
      flamingo = {
        hex = "#eebebe";
        r = 238;
        g = 190;
        b = 190;
      };
      pink = {
        hex = "#f4b8e4";
        r = 244;
        g = 184;
        b = 228;
      };
      mauve = {
        hex = "#ca9ee6";
        r = 202;
        g = 158;
        b = 230;
      };
      red = {
        hex = "#e78284";
        r = 231;
        g = 130;
        b = 132;
      };
      maroon = {
        hex = "#ea999c";
        r = 234;
        g = 153;
        b = 156;
      };
      peach = {
        hex = "#ef9f76";
        r = 239;
        g = 159;
        b = 118;
      };
      yellow = {
        hex = "#e5c890";
        r = 229;
        g = 200;
        b = 144;
      };
      green = {
        hex = "#a6d189";
        r = 166;
        g = 209;
        b = 137;
      };
      teal = {
        hex = "#81c8be";
        r = 129;
        g = 200;
        b = 190;
      };
      sky = {
        hex = "#99d1db";
        r = 153;
        g = 209;
        b = 219;
      };
      sapphire = {
        hex = "#85c1dc";
        r = 133;
        g = 193;
        b = 220;
      };
      blue = {
        hex = "#8caaee";
        r = 140;
        g = 170;
        b = 238;
      };
      lavender = {
        hex = "#babbf1";
        r = 186;
        g = 187;
        b = 241;
      };
      text = {
        hex = "#c6d0f5";
        r = 198;
        g = 208;
        b = 245;
      };
      subtext1 = {
        hex = "#b5bfe2";
        r = 181;
        g = 191;
        b = 226;
      };
      subtext0 = {
        hex = "#a5adce";
        r = 165;
        g = 173;
        b = 206;
      };
      overlay2 = {
        hex = "#949cbb";
        r = 148;
        g = 156;
        b = 187;
      };
      overlay1 = {
        hex = "#838ba7";
        r = 131;
        g = 139;
        b = 167;
      };
      overlay0 = {
        hex = "#737994";
        r = 115;
        g = 121;
        b = 148;
      };
      surface2 = {
        hex = "#626880";
        r = 98;
        g = 104;
        b = 128;
      };
      surface1 = {
        hex = "#51576d";
        r = 81;
        g = 87;
        b = 109;
      };
      surface0 = {
        hex = "#414559";
        r = 65;
        g = 69;
        b = 89;
      };
      base = {
        hex = "#303446";
        r = 48;
        g = 52;
        b = 70;
      };
      mantle = {
        hex = "#292c3c";
        r = 41;
        g = 44;
        b = 60;
      };
      crust = {
        hex = "#232634";
        r = 35;
        g = 38;
        b = 52;
      };
    };

    macchiato = {
      rosewater = {
        hex = "#f4dbd6";
        r = 244;
        g = 219;
        b = 214;
      };
      flamingo = {
        hex = "#f0c6c6";
        r = 240;
        g = 198;
        b = 198;
      };
      pink = {
        hex = "#f5bde6";
        r = 245;
        g = 189;
        b = 230;
      };
      mauve = {
        hex = "#c6a0f6";
        r = 198;
        g = 160;
        b = 246;
      };
      red = {
        hex = "#ed8796";
        r = 237;
        g = 135;
        b = 150;
      };
      maroon = {
        hex = "#ee99a0";
        r = 238;
        g = 153;
        b = 160;
      };
      peach = {
        hex = "#f5a97f";
        r = 245;
        g = 169;
        b = 127;
      };
      yellow = {
        hex = "#eed49f";
        r = 238;
        g = 212;
        b = 159;
      };
      green = {
        hex = "#a6da95";
        r = 166;
        g = 218;
        b = 149;
      };
      teal = {
        hex = "#8bd5ca";
        r = 139;
        g = 213;
        b = 202;
      };
      sky = {
        hex = "#91d7e3";
        r = 145;
        g = 215;
        b = 227;
      };
      sapphire = {
        hex = "#7dc4e4";
        r = 125;
        g = 196;
        b = 228;
      };
      blue = {
        hex = "#8aadf4";
        r = 138;
        g = 173;
        b = 244;
      };
      lavender = {
        hex = "#b7bdf8";
        r = 183;
        g = 189;
        b = 248;
      };
      text = {
        hex = "#cad3f5";
        r = 202;
        g = 211;
        b = 245;
      };
      subtext1 = {
        hex = "#b8c0e0";
        r = 184;
        g = 192;
        b = 224;
      };
      subtext0 = {
        hex = "#a5adcb";
        r = 165;
        g = 173;
        b = 203;
      };
      overlay2 = {
        hex = "#939ab7";
        r = 147;
        g = 154;
        b = 183;
      };
      overlay1 = {
        hex = "#8087a2";
        r = 128;
        g = 135;
        b = 162;
      };
      overlay0 = {
        hex = "#6e738d";
        r = 110;
        g = 115;
        b = 141;
      };
      surface2 = {
        hex = "#5b6078";
        r = 91;
        g = 96;
        b = 120;
      };
      surface1 = {
        hex = "#494d64";
        r = 73;
        g = 77;
        b = 100;
      };
      surface0 = {
        hex = "#363a4f";
        r = 54;
        g = 58;
        b = 79;
      };
      base = {
        hex = "#24273a";
        r = 36;
        g = 39;
        b = 58;
      };
      mantle = {
        hex = "#1e2030";
        r = 30;
        g = 32;
        b = 48;
      };
      crust = {
        hex = "#181926";
        r = 24;
        g = 25;
        b = 38;
      };
    };

    mocha = {
      rosewater = {
        hex = "#f5e0dc";
        r = 245;
        g = 224;
        b = 220;
      };
      flamingo = {
        hex = "#f2cdcd";
        r = 242;
        g = 205;
        b = 205;
      };
      pink = {
        hex = "#f5c2e7";
        r = 245;
        g = 194;
        b = 231;
      };
      mauve = {
        hex = "#cba6f7";
        r = 203;
        g = 166;
        b = 247;
      };
      red = {
        hex = "#f38ba8";
        r = 243;
        g = 139;
        b = 168;
      };
      maroon = {
        hex = "#eba0ac";
        r = 235;
        g = 160;
        b = 172;
      };
      peach = {
        hex = "#fab387";
        r = 250;
        g = 179;
        b = 135;
      };
      yellow = {
        hex = "#f9e2af";
        r = 249;
        g = 226;
        b = 175;
      };
      green = {
        hex = "#a6e3a1";
        r = 166;
        g = 227;
        b = 161;
      };
      teal = {
        hex = "#94e2d5";
        r = 148;
        g = 226;
        b = 213;
      };
      sky = {
        hex = "#89dceb";
        r = 137;
        g = 220;
        b = 235;
      };
      sapphire = {
        hex = "#74c7ec";
        r = 116;
        g = 199;
        b = 236;
      };
      blue = {
        hex = "#89b4fa";
        r = 137;
        g = 180;
        b = 250;
      };
      lavender = {
        hex = "#b4befe";
        r = 180;
        g = 190;
        b = 254;
      };
      text = {
        hex = "#cdd6f4";
        r = 205;
        g = 214;
        b = 244;
      };
      subtext1 = {
        hex = "#bac2de";
        r = 186;
        g = 194;
        b = 222;
      };
      subtext0 = {
        hex = "#a6adc8";
        r = 166;
        g = 173;
        b = 200;
      };
      overlay2 = {
        hex = "#9399b2";
        r = 147;
        g = 153;
        b = 178;
      };
      overlay1 = {
        hex = "#7f849c";
        r = 127;
        g = 132;
        b = 156;
      };
      overlay0 = {
        hex = "#6c7086";
        r = 108;
        g = 112;
        b = 134;
      };
      surface2 = {
        hex = "#585b70";
        r = 88;
        g = 91;
        b = 112;
      };
      surface1 = {
        hex = "#45475a";
        r = 69;
        g = 71;
        b = 90;
      };
      surface0 = {
        hex = "#313244";
        r = 49;
        g = 50;
        b = 68;
      };
      base = {
        hex = "#1e1e2e";
        r = 30;
        g = 30;
        b = 46;
      };
      mantle = {
        hex = "#181825";
        r = 24;
        g = 24;
        b = 37;
      };
      crust = {
        hex = "#11111b";
        r = 17;
        g = 17;
        b = 27;
      };
    };
  };
in {
  options.colours = let
    colourDef = {
      r = mkOption {type = types.int;};
      g = mkOption {type = types.int;};
      b = mkOption {type = types.int;};
      hex = mkOption {type = types.str;};
    };
  in {
    scheme = {
      flavour = mkOption {
        type = types.enum ["latte" "frappe" "macchiatto" "mocha"];
        default = "frappe";
      };
      accent = mkOption {
        type = types.enum [
          "rosewater"
          "flamingo"
          "pink"
          "mauve"
          "red"
          "maroon"
          "peach"
          "yellow"
          "green"
          "teal"
          "sky"
          "sapphire"
          "blue"
          "lavender"
        ];
        default = "pink";
      };
    };

    values = {
      accent = colourDef;
      rosewater = colourDef;
      flamingo = colourDef;
      pink = colourDef;
      mauve = colourDef;
      red = colourDef;
      maroon = colourDef;
      peach = colourDef;
      yellow = colourDef;
      green = colourDef;
      teal = colourDef;
      sky = colourDef;
      sapphire = colourDef;
      blue = colourDef;
      lavender = colourDef;
      text = colourDef;
      subtext1 = colourDef;
      subtext0 = colourDef;
      overlay2 = colourDef;
      overlay1 = colourDef;
      overlay0 = colourDef;
      surface2 = colourDef;
      surface1 = colourDef;
      surface0 = colourDef;
      base = colourDef;
      mantle = colourDef;
      crust = colourDef;
    };
  };

  config.colours.values = schemes.${cfg.scheme.flavour} // {accent = schemes.${cfg.scheme.flavour}.${cfg.scheme.accent};};
}
