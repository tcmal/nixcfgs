{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";

    doe = {
      url = "git+https://git.tardisproject.uk/tcmal/doe.git";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager.url = "github:nix-community/home-manager/release-23.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    lanzaboote = {
      url = "github:nix-community/lanzaboote/v0.3.0";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    home-manager,
    ...
  }: let
    system = "x86_64-linux";
    mkSystem = cfg:
      nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = {
          inherit inputs;
          hmUser = "aria";
        };
        modules = [home-manager.nixosModules.home-manager cfg];
      };
  in {
    nixosConfigurations = {
      clovis = mkSystem ./hosts/clovis.nix;
      casper = mkSystem ./hosts/casper.nix;
    };
  };
}
