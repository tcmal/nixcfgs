from setuptools import setup

setup(
    name="mozidbtools",
    packages=["mozidbtools"],
    version="0.1.0",
    install_requires=["sqlite", "glom", "snappy"],
    entry_points={"console_scripts": ["mozidbedit=mozidbtools.mozidbedit:main"]},
)
