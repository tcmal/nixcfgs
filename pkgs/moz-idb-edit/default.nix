{pkgs ? import <nixpkgs> {}}:
with pkgs.python3Packages;
  buildPythonPackage {
    name = "moz-idb-edit";
    src = ./.;
    propagatedBuildInputs = [pkgs.sqlite python-snappy glom];
    doCheck = false;
  }
