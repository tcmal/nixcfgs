{pkgs ? import <nixpkgs> {}}:
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (ps:
      with ps; [
        pkgs.sqlite
        python-snappy
        glom
      ]))
  ];
}
