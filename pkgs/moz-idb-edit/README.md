# moz-idb-edit

A tool for viewing and editing Mozilla IDB databases, which are used for local storage and for extension settings.

Mostly based on code from [ntninja](https://gitlab.com/ntninja/moz-idb-edit/-/tree/main?ref_type=heads).
