{hmUser, ...}: {
  home-manager.users.${hmUser}.programs.ssh = {
    enable = true;
    includes = ["config-local"];

    matchBlocks = {
      "storage" = {
        host = "storage";
        hostname = "u349899.your-storagebox.de";
        port = 23;
        user = "u349899";
      };

      "tardis" = {
        host = "tardis";
        hostname = "tardisproject.uk";
        user = "tcmal";
      };

      "candelabra" = {
        host = "candelabra";
        hostname = "192.168.1.246";
        user = "root";
        proxyJump = "tardis";
      };
    };
  };
}
