{
  config,
  hmUser,
  ...
}: {
  home-manager.users.${hmUser}.services.syncthing.enable = true;
}
