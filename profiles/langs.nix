selections: let
  langs = {
    rust = {
      pkgs,
      hmUser,
      ...
    }: {
      environment.systemPackages = [pkgs.rustup pkgs.gcc];
      home-manager.users.${hmUser}.home.sessionPath = ["$HOME/.cargo/bin"];
    };

    nix = {pkgs, ...}: {
      boot.binfmt.emulatedSystems = ["aarch64-linux"];
      environment.systemPackages = with pkgs; [alejandra nix-prefetch];
    };

    python = {pkgs, ...}: {
      environment.systemPackages = [
        (pkgs.python3.withPackages (ps:
          with ps; [
            pip
            virtualenv
            black
          ]))
      ];
    };

    go = {
      pkgs,
      hmUser,
      ...
    }: {
      environment.systemPackages = with pkgs; [go gopls];
      home-manager.users.${hmUser}.home = {
        sessionVariables = {
          GOMODCACHE = "$HOME/.go/pkg/mod";
          GOPATH = "$HOME/.go";
        };
        sessionPath = ["$HOME/go/bin"];
      };
    };

    k8s = {
      pkgs,
      hmUser,
      ...
    }: {
      users.users.${hmUser}.extraGroups = ["podman"];
      virtualisation.podman = {
        enable = true;
        dockerCompat = true;
        autoPrune.enable = true;
        dockerSocket.enable = true;
      };

      environment.systemPackages = with pkgs; [
        kubectl
        opentofu
        podman-compose
      ];

      environment.shellAliases = {
        "k" = "kubectl";
        "tf" = "tofu";
        "terraform" = "tofu";
      };
      programs.bash.interactiveShellInit = ''
        complete -o default -F __start_kubectl k
        complete -o default -F __start_tofu tf
        complete -o default -F __start_tofu terraform
      '';
    };

    writing = {pkgs, ...}: {
      environment.systemPackages = [pkgs.zotero];
    };

    agda = {pkgs, ...}: {
      environment.systemPackages = [(pkgs.agda.withPackages (ps: with ps; [standard-library]))];
    };

    virt = {
      pkgs,
      hmUser,
      ...
    }: {
      virtualisation.libvirtd = {
        enable = true;
        onBoot = "ignore";
        parallelShutdown = 3;
      };
      users.users.${hmUser}.extraGroups = ["libvirtd"];
    };
  };
in {imports = map (name: langs.${name}) selections;}
