{hmUser, ...}: {
  home-manager.users.${hmUser}.programs.git = {
    enable = true;
    extraConfig = {
      init.defaultBranch = "main";

      push = {
        autosetupremote = true;
        followtags = "true";
      };
      pull.rebase = true;
      rebase.autostash = true;

      help.autocorrect = "prompt";
      diff.algorithm = "histogram";

      log.date = "iso";
      "gitlab \"git.tardisproject.uk/api/v4\"".user = "tcmal";
    };
    includes = [{path = "~/.gitconfig.local";}];
  };
}
