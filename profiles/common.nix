{
  inputs,
  pkgs,
  hmUser,
  config,
  lib,
  ...
}: let
  inherit (lib) mkForce;
in {
  # Locale setup
  time.timeZone = "Europe/London";
  i18n.defaultLocale = "en_GB.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_GB.UTF-8";
    LC_IDENTIFICATION = "en_GB.UTF-8";
    LC_MEASUREMENT = "en_GB.UTF-8";
    LC_MONETARY = "en_GB.UTF-8";
    LC_NAME = "en_GB.UTF-8";
    LC_NUMERIC = "en_GB.UTF-8";
    LC_PAPER = "en_GB.UTF-8";
    LC_TELEPHONE = "en_GB.UTF-8";
    LC_TIME = "en_GB.UTF-8";
  };

  # Main desktop user
  users.users.${hmUser} = {
    isNormalUser = true;
    description = "Aria";
    extraGroups = ["networkmanager" "wheel" "video"];
  };

  # Honour nixpkgs configuration / overlays
  home-manager.useGlobalPkgs = true;

  # Utilities
  environment.systemPackages = with pkgs; [
    vim
    gitMinimal
    p7zip
    rsync

    python3
    jq
    ripgrep
    btop
    du-dust
    dogdns
  ];

  # Nix configuration
  nixpkgs.config.allowUnfree = true;
  nix = {
    nixPath = [
      "nixpkgs=${inputs.nixpkgs}"
    ];

    settings = {
      # Improve nix store disk usage
      auto-optimise-store = true;

      # Prevents impurities in builds
      sandbox = true;

      experimental-features = ["nix-command" "flakes" "ca-derivations"];
      flake-registry = "";
      trusted-users = [hmUser];

      connect-timeout = 20;
      log-lines = lib.mkDefault 25;

      max-free = lib.mkDefault (3000 * 1024 * 1024);
      min-free = lib.mkDefault (512 * 1024 * 1024);
    };
    gc = {
      automatic = true;
      options = "--delete-older-than 7d";
    };

    registry = {
      nixpkgs.to = {
        owner = "nixos";
        repo = "nixpkgs";
        type = "github";
        rev = inputs.nixpkgs.rev;
      };
    };
  };

  # Config metadata
  system.stateVersion = "23.11";
  home-manager.users.${hmUser}.home.stateVersion = "23.11";

  hardware.enableRedistributableFirmware = true;
  nixpkgs.hostPlatform = "x86_64-linux";

  # Some stuff from the minimal nixos profile

  # Largely unhelpful, so better to install them explicitly as needed
  documentation.doc.enable = false;
  documentation.info.enable = false;
  documentation.nixos.enable = false;
  documentation.man.enable = true;

  # Perl is a default package
  environment.defaultPackages = [];

  # The lessopen package pulls in Perl.
  programs.less.lessopen = null;

  # Unnecessary programs/services
  programs.command-not-found.enable = false;
  services.logrotate.enable = false;
  services.udisks2.enable = false;

  xdg.autostart.enable = mkForce false;
  xdg.sounds.enable = mkForce false;
  xdg.mime.enable = true;

  # Hardening
  boot.tmp.cleanOnBoot = true;
  systemd.enableEmergencyMode = false;

  system.activationScripts.diff = {
    supportsDryActivation = true;
    text = ''
      if [[ -e /run/current-system ]]; then
        echo "--- diff to current-system"
        ${pkgs.nvd}/bin/nvd --nix-bin-dir=${config.nix.package}/bin diff /run/current-system "$systemConfig"
        echo "---"
      fi
    '';
  };
}
