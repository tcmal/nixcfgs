{
  config,
  hmUser,
  pkgs,
  lib,
  ...
}: {
  imports = [
    ./colours.nix
    ./sidebery.nix
  ];

  home-manager.users.${hmUser} = {
    imports = [../../modules/hm/firefox.nix];
    programs.firefox = {
      enable = true;
      policies = let
        lock-false = {
          Value = false;
          Status = "locked";
        };
        lock-true = {
          Value = true;
          Status = "locked";
        };
      in {
        # Privacy stuff
        DisableTelemetry = true;
        DisableFirefoxStudies = true;
        EnableTrackingProtection = {
          Value = true;
          Locked = true;
          Cryptomining = true;
          Fingerprinting = true;
        };
        DisablePocket = true;
        OverrideFirstRunPage = "";
        OverridePostUpdatePage = "";
        DontCheckDefaultBrowser = true;
        DisplayBookmarksToolbar = "never"; # alternatives: "always" or "newtab"
        DisplayMenuBar = "default-off"; # alternatives: "always", "never" or "default-on"
        SearchBar = "unified"; # alternative: "separate"

        DisableFirefoxAccounts = false;
        DisableFirefoxScreenshots = false;
        DisableAccounts = false;

        PasswordManagerEnabled = false;

        SearchEngines.Remove = ["Google"];

        Preferences = {
          # Based on github.com/arkenfox/user.js
          "extensions.pocket.enabled" = lock-false;

          # Cleanup new tab page
          "browser.startup.page" = 1;
          "browser.newtabpage.pinned" = "[]";
          "browser.newtabpage.activity-stream.feeds.section.topstories" = lock-false;
          "browser.newtabpage.activity-stream.feeds.snippets" = lock-false;
          "browser.newtabpage.activity-stream.feeds.topsites" = lock-false;
          "browser.newtabpage.activity-stream.section.highlights.includePocket" = lock-false;
          "browser.newtabpage.activity-stream.section.highlights.includeBookmarks" = lock-false;
          "browser.newtabpage.activity-stream.section.highlights.includeDownloads" = lock-false;
          "browser.newtabpage.activity-stream.section.highlights.includeVisited" = lock-false;
          "browser.newtabpage.activity-stream.showSponsored" = lock-false;
          "browser.newtabpage.activity-stream.showSponsoredTopSites" = lock-false;
          "browser.newtabpage.activity-stream.system.showSponsored" = lock-false;

          # Use mozilla geolocation
          "geo.provider.network.url" = "https://location.services.mozilla.com/v1/geolocate?key=%MOZILLA_API_KEY%";
          "geo.provider.use_gpsd" = lock-false;
          "geo.provider.use_geoclue" = lock-false;

          # Recommendations
          "extensions.getAddons.showPane" = lock-false;
          "extensions.htmlaboutaddons.recommendations.enabled" = lock-false;
          "browser.discovery.enabled" = lock-false;
          "browser.shopping.experience2023.enabled" = lock-false;

          # Telemetry
          "datareporting.policy.dataSubmissionEnabled" = lock-false;
          "datareporting.healthreport.uploadEnabled" = lock-false;
          "toolkit.telemetry.unified" = lock-false;
          "toolkit.telemetry.enabled" = lock-false;
          "toolkit.telemetry.server" = "data:";
          "toolkit.telemetry.archive.enabled" = lock-false;
          "toolkit.telemetry.newProfilePing.enabled" = lock-false;
          "toolkit.telemetry.shutdownPingSender.enabled" = lock-false;
          "toolkit.telemetry.updatePing.enabled" = lock-false;
          "toolkit.telemetry.bhrPing.enabled" = lock-false;
          "toolkit.telemetry.firstShutdownPing.enabled" = lock-false;
          "toolkit.telemetry.coverage.opt-out" = lock-true;
          "toolkit.coverage.opt-out" = lock-true;
          "toolkit.coverage.endpoint.base" = "";
          "browser.ping-centre.telemetry" = lock-false;
          "browser.newtabpage.activity-stream.feeds.telemetry" = lock-false;
          "browser.newtabpage.activity-stream.telemetry" = lock-false;

          # Studies
          "app.shield.optoutstudies.enabled" = lock-false;
          "app.normandy.enabled" = lock-false;
          "app.normandy.api_url" = "";

          # Crash reports
          "breakpad.reportURL" = "";
          "browser.tabs.crashReporting.sendReport" = lock-false;
          "browser.crashReports.unsubmittedCheck.autoSubmit2" = lock-false;

          # Safe browsing: On, but don't send downloads to google
          "browser.safebrowsing.downloads.remote.enabled" = lock-false;

          # Search bar
          "browser.urlbar.speculativeConnect.enabled" = lock-false;
          "browser.urlbar.suggest.quicksuggest.nonsponsored" = lock-false;
          "browser.urlbar.suggest.quicksuggest.sponsored" = lock-false;
          "browser.search.suggest.enabled" = lock-false;
          "browser.urlbar.suggest.searches" = lock-false;
          "browser.urlbar.trending.featureGate" = lock-false;
          "browser.urlbar.addons.featureGate" = lock-false;
          "browser.urlbar.mdn.featureGate" = lock-false;
          "browser.urlbar.pocket.featureGate" = lock-false;
          "browser.urlbar.weather.featureGate" = lock-false;

          # Autofill
          "browser.formfill.enable" = lock-false;
          "signon.autofillForms" = lock-false;
          "signon.formlessCapture.enabled" = lock-false;

          # Better SSL error display
          "browser.xul.error_pages.expert_bad_cert" = lock-true;

          # Miscellaneous
          "font.default.x-western" = "sans-serif";
          "browser.uitour.enabled" = lock-false;
          "pdfjs.enableScripting" = lock-false;
          "browser.contentblocking.category" = "strict";
          "privacy.donottrackheader.enabled" = lock-true;
          "privacy.globalprivacycontrol.enabled" = lock-true;
          "privacy.globalprivacycontrol.was-ever-enabled" = lock-true;
          "signon.rememberSignons" = lock-false;
          "browser.toolbars.bookmarks.visibility" = "never";
          "browser.startup.homepage_override.mstone" = "ignore";
          "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons" = lock-false;
          "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features" = lock-false;
          "browser.messaging-system.whatsNewPanel.enabled" = lock-false;
          "toolkit.legacyUserProfileCustomizations.stylesheets" = lock-true;
        };

        # Credit: https://discourse.nixos.org/t/declare-firefox-extensions-and-settings/36265/17
        ExtensionSettings = with builtins; let
          extension = shortId: uuid: {
            name = uuid;
            value = {
              install_url = "https://addons.mozilla.org/en-US/firefox/downloads/latest/${shortId}/latest.xpi";
              installation_mode = "normal_installed";
            };
          };
        in
          listToAttrs [
            (extension "bitwarden-password-manager" "{446900e4-71c2-419f-a6a7-df9c091e268b}")
            (extension "ublock-origin" "uBlock0@raymondhill.net")
            (extension "sidebery" "{3c078156-979c-498b-8990-85f7987dd929}")
            (extension "indie-wiki-buddy" "{cb31ec5d-c49a-4e5a-b240-16c767444f62}")
            (extension "consent-o-matic" "gdpr@cavi.au.dk")
            (extension "sponsorblock" "sponsorBlocker@ajay.app")
            (extension "firefox-color" "FirefoxColor@mozilla.com")
            (extension "styl-us" "{7a7a4a92-a2a0-41d1-9fd7-1e92480d612d}")
          ];
        # To add additional extensions, find it on addons.mozilla.org, find
        # the short ID in the url (like https://addons.mozilla.org/en-US/firefox/addon/!SHORT_ID!/)
        # Then, download the XPI by filling it in to the install_url template, unzip it,
        # run `jq .browser_specific_settings.gecko.id manifest.json` or
        # `jq .applications.gecko.id manifest.json` to get the UUID
      };

      profiles.default = {
        id = 0;
        name = "Default";
        isDefault = true;
        search = {
          default = "DuckDuckGo";
          engines = {
            "Nix Packages" = {
              urls = [
                {
                  template = "https://search.nixos.org/packages";
                  params = [
                    {
                      name = "type";
                      value = "packages";
                    }
                    {
                      name = "query";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];

              icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
              definedAliases = ["@np"];
            };

            "Amazon.co.uk".metaData.hidden = true;
            "Bing".metaData.hidden = true;
            "Google".metaData.hidden = true;
            "eBay".metaData.hidden = true;
          };
        };
        userChrome = let
          flavour = config.services.ariade.catppuccin.flavour;
        in ''
          #TabsToolbar{ visibility: collapse !important }

          #sidebar-header {
            display: none;
          }
        '';
      };
    };

    home.file = {
      # Workaround: Firefox keeps re-creating this file, so we must force-overwrite it
      ".mozilla/firefox/default/search.json.mozlz4".force = lib.mkForce true;
    };
  };

  # Disable speech synthesis, as it's a huge dependency and not super useful to me
  nixpkgs.overlays = [
    (final: prev:
      prev
      // {
        firefox = prev.firefox.override {
          cfg.speechSynthesisSupport = false;
        };
      })
  ];
}
