;; A lot of this is taken from Lambda Emacs: https://github.com/Lambda-Emacs/lambda-emacs

;;;; Speed up startup
;; Help speed up emacs initialization See
;; https://blog.d46.us/advanced-emacs-startup/ and
;; http://tvraman.github.io/emacspeak/blog/emacs-start-speed-up.html and
;; https://www.reddit.com/r/emacs/comments/3kqt6e/2_easy_little_known_steps_to_speed_up_emacs_start/
;; This will be set back to normal at the end of the init file

(defvar original-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

;;;; Native Comp
;; Silence nativecomp warnings popping up
(setopt native-comp-async-report-warnings-errors nil)

;; Native-comp settings
(setopt native-comp-speed 2)
(setopt native-comp-deferred-compilation t)
(setq byte-compile-warnings '(not free-vars unresolved noruntime lexical make-local obsolete))

;;;; Garbage collection
;; Defer garbage collection further back in the startup process. We'll lower
;; this to a more reasonable number at the end of the init process (i.e. at end of
;; init.el)

(setq gc-cons-threshold most-positive-fixnum)

;;;; Clean View
;; Resizing the Emacs frame can be an expensive part of changing the
;; font. By inhibiting this, we easily halve startup times with fonts that are
;; larger than the system default.
(setopt frame-inhibit-implied-resize t
        frame-title-format "\n"
        inhibit-startup-screen t
        inhibit-startup-message t
        inhibit-splash-screen t
        initial-scratch-message nil)

(setopt tool-bar-mode nil
        scroll-bar-mode nil)

;; Fundamental mode at startup.
;; This helps with load-time since no extra libraries are loaded.
(setopt initial-major-mode 'fundamental-mode)

;; Echo buffer -- don't display any message
;; https://emacs.stackexchange.com/a/437/11934
(defun display-startup-echo-area-message ()
  (message ""))

;;;; Add rest of config files to load path
(defconst config-emacs-dir (expand-file-name user-emacs-directory)
  "Path to the emacs.d directory.")

(defconst config-etc-dir (concat config-emacs-dir "etc/")
  "Path to local configuration.")

(defconst config-cache-dir (concat config-emacs-dir "cache/")
  "Path to local cache/temporary files")

(defconst config-library-dir (concat config-emacs-dir "config/")
  "Path to config files.")

(eval-and-compile
  (progn
    (push config-library-dir load-path)))


;;;; Bootstrap Package System
;; Load the package-system.
(require 'package)

;;;; Package Archives
;; See https://protesilaos.com/codelog/2022-05-13-emacs-elpa-devel/ for discussion
(setopt package-archives
        '(("elpa" . "https://elpa.gnu.org/packages/")
          ("nongnu" . "https://elpa.nongnu.org/nongnu/")
          ("melpa" . "https://melpa.org/packages/"))

        ;; Set location of package directory
        package-user-dir (expand-file-name "elpa/" config-cache-dir))

;; Make sure the elpa/ folder exists after setting it above.
(unless (file-exists-p package-user-dir)
  (mkdir package-user-dir t))
(setopt package-quickstart-file (expand-file-name "package-quickstart.el" config-cache-dir))
