{
  pkgs,
  config,
  hmUser,
  ...
}: {
  home-manager.users.${hmUser} = let
    emacsRaw = pkgs.emacs29;
    # Remove pure emacs desktop file, since we use emacsclient
    emacs = pkgs.runCommand "emacs-nodesktop" {} ''
      cp -rs --no-preserve=mode ${emacsRaw} $out;
      rm $out/share/applications/emacs.desktop;
      rm $out/share/applications/emacs-mail.desktop;
    '';
  in {
    home.packages = [pkgs.xclip];

    programs.emacs = {
      enable = true;
      package = emacs;
    };
    services.emacs = {
      enable = true;
      package = emacs;
      socketActivation.enable = true;
      defaultEditor = true;
      client = {
        enable = true;
      };
    };

    home.file = {
      ".emacs.d" = {
        source = ./.;
        recursive = true;
      };
      ".emacs.d/typst-ts-mode" = {
        source = pkgs.fetchgit {
          url = "https://git.sr.ht/~meow_king/typst-ts-mode";
          rev = "4ea3001a8d7fbd46a3a37f481f22707437b3295c";
          hash = "sha256-t0oraoiQyND6nAnIiK39kIxbWpmYlqegriS5ktbcv0A=";
        };
      };
      ".emacs.d/config/config-theme.el" = {
        text = ''
          (use-package catppuccin-theme
            :config
            (load-theme 'catppuccin :no-confirm)
            (setq catppuccin-flavor '${config.colours.scheme.flavour})
            (catppuccin-reload))

          (provide 'config-theme)
        '';
      };
    };
  };
}
