;;;; Use-Package Settings
(require 'use-package)
(use-package use-package
  :custom
  (use-package-always-defer nil)
  (use-package-expand-minimally nil)
  (use-package-always-ensure t)
  (use-package-enable-imenu-support t))

;;;;; Auto-compile
;; Automatically byte-recompile changed elisp libraries
(use-package auto-compile
  :ensure t
  :defer 1
  :custom
  (auto-compile-display-buffer nil)
  (auto-compile-mode-line-counter nil)
  (auto-compile-use-mode-line nil)
  (auto-compile-update-autoloads t)
  :config
  (auto-compile-on-load-mode)
  (auto-compile-on-save-mode))

;;;; Pre-requisite setup
;; Used as a leader throughout, so we make it a keymap and bind it to the leader here
(defvar leader-map
  (make-sparse-keymap)
  "Keys with a SPC prefix")

(with-eval-after-load 'evil
  (unbind-key "SPC" evil-motion-state-map)
  (define-key evil-motion-state-map (kbd "SPC") leader-map))

(cl-dolist (mod (list
                 'config-clean
                 'config-appearance
                 'config-help
                 'config-misc

                 'config-evil
                 'config-navigation
                 'config-completion

                 'config-code
                 'config-langs

                 'config-org
                 'config-org-agenda
                 'config-org-roam))
  (require mod))

;;;; After Startup
;; Reset variables we changed for faster startup
(add-hook 'emacs-startup-hook (lambda ()
                                (setq file-name-handler-alist original-file-name-handler-alist)
                                (setq gc-cons-threshold 800000)))
