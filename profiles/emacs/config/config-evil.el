;;;; Vim Emulation
(use-package evil
  :defer nil
  :config
  (setq evil-want-integration t
        evil-undo-system 'undo-fu
        evil-want-keybinding nil
        evil-want-C-u-scroll t
        evil-want-C-d-scroll t
        evil-want-C-i-jump nil
        evil-emacs-state-cursor '(bar . 0 )
        evil-cross-lines t
        evil-shift-width 4)
  (evil-mode 1)
  :bind (:map evil-motion-state-map
              ("C-s" . 'save-buffer)
              ("C-w" . 'kill-current-buffer)
              ("j" . 'evil-next-visual-line)
              ("k" . 'evil-previous-visual-line))
  :bind (:map evil-insert-state-map
              ("C-g" . 'evil-normal-state)
              ("C-SPC" . 'completion-at-point)
              ("C-s" . 'mal/save-normal)))

(defun mal/save-normal ()
  """Save current buffer then return to normal mode"""
  (interactive)
  (save-buffer)
  (evil-normal-state))

;; Sets up more modes
(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package evil-surround
  :config
  (global-evil-surround-mode))

;;;; Undo
(use-package undo-fu)

(provide 'config-evil)
