;;;; Vertical completion
(use-package vertico
  :defer nil
  :config
  (setq vertico-cycle t)
  (vertico-mode)
  :bind (:map vertico-map
               ("C-j" . vertico-next)
               ("C-k" . vertico-previous)))

;; More information
(use-package marginalia
  :config
  (setq marginalia-annotators '(marginalia-annotators-heavy
                                   marginalia-annotators-light nil))
  (marginalia-mode))

;; Better search results
(use-package orderless
  :config
  (setq completion-styles '(orderless basic partial-completion emacs22)
           completion-ignore-case t))

;;;; At-point completion
(use-package corfu
  :config
  (setq corfu-cycle t
        corfu-auto nil
        corfu-auto-delay nil)
  :hook ((prog-mode text-mode shell-mode eshell-mode) . corfu-mode)
  :bind (:map corfu-map
              ("C-j" . 'corfu-next)
              ("C-k" . 'corfu-previous)))

(provide 'config-completion)
