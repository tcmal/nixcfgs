;;;; Org mode
(use-package org
  :mode ("\\.org\\'" . org-mode)
  :init
  (setq org-confirm-babel-evaluate nil
        org-goto-interface 'outline-path-completion
        org-cycle-separator-lines -1
        org-M-RET-may-split-line nil
        org-log-into-drawer t
        org-auto-align-tags nil
        org-tags-column 0

        org-image-actual-width '(600)
        org-preview-latex-image-directory (expand-file-name "~/.cache/emacs/ltximg/")
        org-format-latex-options '(:foreground default :background default :scale 2 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
                                               ("begin" "$1" "$" "$$" "\\(" "\\["))
        org-startup-with-inline-images t
        org-hide-emphasis-markers t)
  :config
  (evil-define-key 'motion org-mode-map (kbd "RET") '+org/dwim-at-point)
  :bind (:map org-mode-map
              ("C-j" . 'org-next-visible-heading)
              ("C-k" . 'org-previous-visible-heading)

              ("M-h" . 'org-do-promote)
              ("M-l" . 'org-do-demote)
              ("M-S-h" . 'org-promote-subtree)
              ("M-S-l" . 'org-demote-subtree)
              ("M-j" . 'org-metadown)
              ("M-k" . 'org-metaup)

              ("<M-return>" . (lambda (&rest _)
                             (interactive)
                             (+org/meta-return-dwim)
                             (evil-append 1)))
              ("<M-S-return>" . (lambda (&rest _)
                               (interactive)
                               (org-insert-heading-respect-content)
                               (evil-append 1)))))

;; DWIM functions (from doom emacs)
(defun +org/meta-return-dwim (&rest _)
  (interactive)
  (let* ((context (org-element-context))
         (type (org-element-type context)))
    (while (and context (memq type '(verbatim code bold italic underline strike-throug subscript superscript)))
      (setq context (org-element-property :parent context
                                          type (org-element-type context))))
    (pcase type
      (`headline
       (cond ((or (org-element-property :todo-type context)
                  (org-element-property :scheduled context))
              (evil-org-org-insert-todo-heading-respect-content-below))
             (t (org-meta-return))))
      (_ (org-meta-return)))))

(defun +org/dwim-at-point (&optional arg)
  "Do-what-I-mean at point.
If on a:
- checkbox list item or todo heading: toggle it.
- headline: cycle ARCHIVE subtrees, toggle latex fragments and inline images in
  subtree; update statistics cookies/checkboxes and ToCs.
- footnote reference: jump to the footnote's definition
- footnote definition: jump to the first reference of this footnote
- table-row or a TBLFM: recalculate the table's formulas
- table-cell: clear it and go into insert mode. If this is a formula cell,
  recaluclate it instead.
- babel-call: execute the source block
- statistics-cookie: update it.
- latex fragment: toggle it.
- link: follow it
- otherwise, refresh all inline images in current tree."
  (interactive "P")
  (if (button-at (point))
      (call-interactively #'push-button)
    (let* ((context (org-element-context))
           (type (org-element-type context)))
      ;; skip over unimportant contexts
      (while (and context (memq type '(verbatim code bold italic underline strike-throug subscript superscript)))
        (setq context (org-element-property :parent context
                                            type (org-element-type context))))
      (pcase type
        ((or `citation `citation-reference)
         (org-cite-follow context arg))

        (`headline
         (cond ((and (fboundp 'toc-org-insert-toc)
                     (member "TOC" (org-get-tags)))
                (toc-org-insert-toc)
                (message "Updating table of contents"))
               ((string= "ARCHIVE" (car-safe (org-get-tags)))
                (org-force-cycle-archived))
               ((or (org-element-property :todo-type context)
                    (org-element-property :scheduled context))
                (org-todo
                 (if (eq (org-element-property :todo-type context) 'done)
                     (or (car (+org-get-todo-keywords-for (org-element-property :todo-keyword context)))
                         'todo)
                   'done))))
         ;; Update any metadata or inline previews in this subtree
         (org-update-checkbox-count)
         (org-update-parent-todo-statistics)
         (when (and (fboundp 'toc-org-insert-toc)
                    (member "TOC" (org-get-tags)))
           (toc-org-insert-toc)
           (message "Updating table of contents"))
         (let* ((beg (if (org-before-first-heading-p)
                         (line-beginning-position)
                       (save-excursion (org-back-to-heading) (point))))
                (end (if (org-before-first-heading-p)
                         (line-end-position)
                       (save-excursion (org-end-of-subtree) (point))))
                (overlays (ignore-errors (overlays-in beg end)))
                (latex-overlays
                 (cl-find-if (lambda (o) (eq (overlay-get o 'org-overlay-type) 'org-latex-overlay))
                             overlays))
                (image-overlays
                 (cl-find-if (lambda (o) (overlay-get o 'org-image-overlay))
                             overlays)))
           (+org--toggle-inline-images-in-subtree beg end)
           (if (or image-overlays latex-overlays)
               (org-clear-latex-preview beg end)
             (org--latex-preview-region beg end))))

        (`clock (org-clock-update-time-maybe))

        (`footnote-reference
         (org-footnote-goto-definition (org-element-property :label context)))

        (`footnote-definition
         (org-footnote-goto-previous-reference (org-element-property :label context)))

        ((or `planning `timestamp)
         (org-follow-timestamp-link))

        ((or `table `table-row)
         (if (org-at-TBLFM-p)
             (org-table-calc-current-TBLFM)
           (ignore-errors
             (save-excursion
               (goto-char (org-element-property :contents-begin context))
               (org-call-with-arg 'org-table-recalculate (or arg t))))))

        (`table-cell
         (org-table-blank-field)
         (org-table-recalculate arg)
         (when (and (string-empty-p (string-trim (org-table-get-field)))
                    (bound-and-true-p evil-local-mode))
           (evil-change-state 'insert)))

        (`babel-call
         (org-babel-lob-execute-maybe))

        (`statistics-cookie
         (save-excursion (org-update-statistics-cookies arg)))

        ((or `src-block `inline-src-block)
         (org-babel-execute-src-block arg))

        ((or `latex-fragment `latex-environment)
         (org-latex-preview arg))

        (`link
         (let* ((lineage (org-element-lineage context '(link) t))
                (path (org-element-property :path lineage)))
           (if (or (equal (org-element-property :type lineage) "img")
                   (and path (image-type-from-file-name path)))
               (+org--toggle-inline-images-in-subtree
                (org-element-property :begin lineage)
                (org-element-property :end lineage))
             (org-open-at-point arg))))

        ((guard (org-element-property :checkbox (org-element-lineage context '(item) t)))
         (let ((match (and (org-at-item-checkbox-p) (match-string 1))))
           (org-toggle-checkbox (if (equal match "[ ]") '(16)))))

        (_
         (if (or (org-in-regexp org-ts-regexp-both nil t)
                 (org-in-regexp org-tsr-regexp-both nil  t)
                 (org-in-regexp org-link-any-re nil t))
             (call-interactively #'org-open-at-point)
           (+org--toggle-inline-images-in-subtree
            (org-element-property :begin context)
            (org-element-property :end context))))))))

(defun +org-get-todo-keywords-for (&optional keyword)
  "Returns the list of todo keywords that KEYWORD belongs to."
  (when keyword
    (cl-loop for (type . keyword-spec)
             in (cl-remove-if-not #'listp org-todo-keywords)
             for keywords =
             (mapcar (lambda (x) (if (string-match "^\\([^(]+\\)(" x)
                                     (match-string 1 x)
                                   x))
                     keyword-spec)
             if (eq type 'sequence)
             if (member keyword keywords)
             return keywords)))

(defun +org--toggle-inline-images-in-subtree (&optional beg end refresh)
  "Refresh inline image previews in the current heading/tree."
  (let ((beg (or beg
                 (if (org-before-first-heading-p)
                     (line-beginning-position)
                   (save-excursion (org-back-to-heading) (point)))))
        (end (or end
                 (if (org-before-first-heading-p)
                     (line-end-position)
                   (save-excursion (org-end-of-subtree) (point)))))
        (overlays (cl-remove-if-not (lambda (ov) (overlay-get ov 'org-image-overlay))
                                    (ignore-errors (overlays-in beg end)))))
    (dolist (ov overlays nil)
      (delete-overlay ov)
      (setq org-inline-image-overlays (delete ov org-inline-image-overlays)))
    (when (or refresh (not overlays))
      (org-display-inline-images t t beg end)
      t)))

;; Evil keybindings
(use-package evil-org
  :hook ((org-mode org-agenda-mode) . evil-org-mode)
  :config
  (require 'evil-org-agenda)
  (evil-org-set-key-theme '(navigation todo insert textobjects additional))
  (evil-org-agenda-set-keys))


;;;; Org modern
;; A nice looking theme for org mode
(use-package org-modern
  :hook ((org-mode . org-modern-mode)
         (org-agenda-mode . org-modern-agenda))
  :config
  (setq org-pretty-entities t
        org-ellipsis " ▾"
        org-modern-star '("○" "○" "○" "○" "○"))
    (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-table nil  :inherit 'fixed-pitch)
    (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
    (set-face-attribute 'org-code nil   :inherit 'fixed-pitch)
    (set-face-attribute 'org-verbatim nil :inherit 'org-code)
    (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch) :height 0.6)
    (set-face-attribute 'org-drawer nil :inherit '(shadow fixed-pitch) :height 0.6)
    (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-block-end-line nil :inherit 'fixed-pitch))

;; Text centering and visual wrapping
(use-package visual-fill-column
  :hook ((org-mode org-agenda-mode) . visual-fill-column-mode)
  :init (setq visual-fill-column-width 70
              visual-fill-column-center-text t))

;; Show emphasis markers when near them
(use-package org-appear
  :hook (org-mode . org-appear-mode))

;; Setup some other things
(defun mal/org-mode-setup ()
  (interactive)
  ;; Fonts
  (variable-pitch-mode)
  (setq line-spacing 3)

  ;; Screen cleanup
  (display-line-numbers-mode 0)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (text-scale-set 1.3)
  (org-fold-hide-drawer-all))

(with-eval-after-load "org-modern"
  (add-hook 'org-modern-mode-hook 'mal/org-mode-setup))

;;;; Tempo
;; Provides some shortcuts for org syntax
(use-package org
  :config
  (require 'org-tempo)
    (add-to-list 'org-structure-template-alist '("q" . "quote"))
    (add-to-list 'org-structure-template-alist '("text" . "src text"))
    (add-to-list 'org-structure-template-alist '("cpp" . "src c++"))
    (add-to-list 'org-structure-template-alist '("sh" . "src sh"))
    (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
    (add-to-list 'org-structure-template-alist '("py" . "src python"))
    (add-to-list 'org-structure-template-alist '("ag" . "src agda"))
    (add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))
    (add-to-list 'org-structure-template-alist '("json" . "src json")))

;;;; Clipboard shortcut
(defun org-insert-clipboard-image ()
  "Insert an image from the clipboard, placing the png in the same directory and inserting a link into the current (org) document"
  (interactive)
  (setq filename
        (concat
         (make-temp-name
          (concat (file-name-nondirectory (buffer-file-name))
                  "_"
                  (format-time-string "%Y%m%d_%H%M%S_")) ) ".png"))
  (shell-command (concat "xclip -l 0 -selection clipboard -t image/png -o > \"" filename "\""))
  (insert (concat "[[./" filename "]]"))
  (org-display-inline-images))

(provide 'config-org)
