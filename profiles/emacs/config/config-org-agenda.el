;;;; TODOs
(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "WAIT(w)" "|" "DONE(d!)")
        (sequence "|" "BACK(b)")))

(setq org-todo-keyword-faces
      '(("NEXT" . (:foreground "orange red" :weight bold))
        ("WAIT" . (:foreground "HotPink2" :weight bold))
        ("BACK" . (:foreground "MediumPurple3" :weight bold))))

(setq org-tag-alist
      '((:startgroup)
        (:endgroup)
        ("@home" . ?H)
        ("@school" . ?W)
        ("@societies" . ?W)
        ("@career" . ?W)))

;;;; Agenda
(use-package org
  :commands org-agenda
  :init
  (setq org-agenda-block-separator "----"
        org-agenda-sorting-strategy '((agenda priority-down timestamp-up category-keep)
                                      (todo priority-down timestamp-up category-keep)
                                      (tags priority-down time-up timestamp-up category-keep)
                                      (search priority-down timestamp-up category-keep))

        org-agenda-window-setup 'current-window
        org-agenda-span 'day
        org-agenda-start-with-log-mode t
        org-agenda-remove-tags t
        org-agenda-prefix-format '((agenda . " %i %-12:c%?-12t% s")
                                   (timeline . "%i %c %s")
                                   (todo . " %i %-12:c %-12:(mal/task-scheduling-info)")
                                   (tags . " %i %-12:c%?-12t% s")
                                   (search . " %i %-12:c?-12t% s"))
        )
  :config
  (add-hook 'org-agenda-mode-hook (lambda () (display-line-numbers-mode 0))))
(define-key leader-map (kbd "o") 'org-agenda)

;; Display deadline or scheduled time
(defun mal/task-scheduling-info ()
  (let* ((deadline (org-get-deadline-time (point)))
         (scheduled (org-get-scheduled-time (point)))
         (time (or scheduled deadline)))
    (cond (time (format-time-string "%a %d %b" time))
          (t ""))))

;; Super agenda
;; Provides views with grouped tasks
(use-package org-super-agenda
  :after org
  :init
  (setq org-super-agenda-header-map (make-sparse-keymap)
        org-super-agenda-groups '((:name "School"
                                         :tag "@school")
                                  (:name "Societies"
                                         :tag "@societies")
                                  (:name "Home"
                                         :tag "@home"))

        org-agenda-custom-commands `(("d" "Dashboard"
                                      ((agenda "" ((org-deadline-warning-days 3)
                                                   (org-super-agenda-groups (cons '(:name "Today"
                                                                                          :time-grid t
                                                                                          :scheduled "today")
                                                                                  org-super-agenda-groups))))
                                       (todo "NEXT"
                                             ((org-agenda-overriding-header "Next Actions")))
                                       (todo ""
                                             ((org-agenda-overriding-header "Backlog")))))
                                     ("n" "Next Tasks"
                                      ((todo "NEXT"
                                             ((org-agenda-overriding-header "Next Tasks")))
                                       (agenda "" ((org-deadline-warning-days 0)
                                                   (org-super-agenda-groups (cons '(:name "Today"
                                                                                          :time-grid t
                                                                                          :scheduled "today")
                                                                                  org-super-agenda-groups))
                                                   (org-scheduled-past-days 1)
                                                   (org-deadline-past-days 1)))))

                                     ("r" "Revision"
                                      ((todo "HARD"
                                             ((org-agenda-overriding-header "Hard")))
                                       (todo "FINE"
                                             ((org-agenda-overriding-header "Fine")))
                                       (todo "GOOD"
                                             ((org-agenda-overriding-header "Good")))
                                       (todo "EASY"
                                             ((org-agenda-overriding-header "Easy"))))))
        org-columns-default-format "%TODO %25ITEM %DEADLINE %SCHEDULED")
  (org-super-agenda-mode 1))

(provide 'config-org-agenda)
