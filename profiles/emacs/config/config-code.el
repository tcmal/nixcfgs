;;;; Indentation
;; 4 spaces, no tabs
(setq-default tab-width 4)
(setq-default evil-shift-width tab-width)
(setq-default indent-tabs-mode nil)

;; Clean whitespace
(use-package ws-butler
  :hook ((prog-mode text-mode) . ws-butler-mode))

;;;; Commenting
(use-package evil-nerd-commenter
  :bind ("C-/" . 'evilnc-comment-or-uncomment-lines))

;;;; Parentheses
;; Smart auto-insert
(use-package smartparens
  :config
  (require 'smartparens-config)
  :hook (prog-mode . smartparens-mode))

;; Colour pairs differently
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;;;; .envrc sourcing
(use-package direnv
  :config
  (setq direnv-always-show-summary nil)
  (direnv-mode))

;;;; LSP support
(use-package eglot
  :commands eglot
  :init
  (setq eldoc-echo-area-prefer-doc-buffer t)
  :config
  (add-hook 'eglot-managed-mode-hook #'mal/eglot-keys)
  :bind (:map eglot-mode-map
              ;; Show docs at point
              ("<normal-state> K" . 'eldoc)))

(defun mal/eglot-keys ()
  (if (eglot-managed-p)
      (progn (eldoc-mode -1) ;; Disable on hover documentation
             ;; Extra binds for using LSP
             (define-key evil-motion-state-local-map (kbd "SPC l a") 'eglot-code-actions)
             (define-key evil-motion-state-local-map (kbd "SPC a") 'consult-eglot-symbols)
             (define-key evil-motion-state-local-map (kbd "SPC l r") 'eglot-rename))))

;; Workspace search
(use-package consult-eglot
  :commands consult-eglot-symbols)

;; Start LSP bind
(define-key leader-map (kbd "l l") 'eglot)

;;;; Magit
(defun mal/git-commit-setup ()
  (insert "\n\n# type(scope): <description>\n# types: fix, feat, chore, ci, docs, style, refactor, perf, test\n# use ! for breaking changes")
  (beginning-of-buffer))

(use-package magit
  :commands 'magit-status
  :config
  (add-hook 'git-commit-setup-hook 'mal/git-commit-setup)
  (setq forge-add-default-bindings nil
        magit-show-long-lines-warning nil))

(use-package forge
  :after magit
  :config
  (add-to-list 'forge-alist '("git.tardisproject.uk" "git.tardisproject.uk/api/v4" "git.tardisproject.uk" forge-gitlab-repository)))
(define-key leader-map "g" 'magit-status)

;;;; Format all
(use-package format-all
  :defer t
  :init
  (autoload 'format-all-mode "format-all" nil t)
  (setq format-all-formatters '()))

(provide 'config-code)
