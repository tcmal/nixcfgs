;;;; No littering
(use-package no-littering
  :init
  (setq no-littering-etc-directory (expand-file-name "config/" config-etc-dir)
        no-littering-var-directory (expand-file-name "data/" config-cache-dir)))

(setq user-emacs-directory (expand-file-name "~/.cache/emacs/")
      url-history-file (expand-file-name "url/history" user-emacs-directory)
      backup-directory-alist `(("." . "~/.cache/emacs/saves"))
      create-lockfiles nil)

;;;; Set custom file location
(setq custom-file (expand-file-name "custom.el" config-cache-dir))
(load custom-file t)

;;;; Set exec path from shell PATH
(defun update-path ()
  (interactive)
  (let ((path-from-shell (replace-regexp-in-string
                          "[ \t\n]*$" "" (shell-command-to-string
                                          "$SHELL --login -c 'echo $PATH'"
                                          ))))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))

(update-path)

(provide 'config-clean)
