;;;; Org Roam
(use-package org-roam
  :init
  (setq org-roam-directory (file-truename "~/notes")
        org-roam-dailies-directory "daily/")
  :bind (:map leader-map
              ("r c" . 'org-roam-capture)
              ("r f" . 'org-roam-node-find)
              ("r a" . 'org-roam-alias-add)
              ("r A" . 'org-roam-alias-remove)
              ("r l" . 'org-roam-node-insert)
              ("r i" . 'org-id-get-create)
              ("r d" . 'org-roam-dailies-capture-today)
              ("r t" . 'org-roam-buffer-toggle))
  :config
  (org-roam-db-autosync-mode)
  (add-hook 'org-follow-link-hook 'delete-other-windows))

(provide 'config-org-roam)
