;;;; Keybinding help
(use-package which-key
  :config
  (setq which-key-idle-delay 0.3)
  (which-key-mode))

;;;; Improves help pages
(use-package helpful
  :bind (([remap describe-function] . helpful-function)
         ([remap describe-symbol] . helpful-symbol)
         ([remap describe-variable] . helpful-variable)
         ([remap describe-key] . helpful-key)
         ([remap describe-command] . helpful-describe-command)))

(provide 'config-help)
