;;;; Rust
(use-package rust-mode
  :mode "\\.rs\\'"
  :config
  (setq rust-format-on-save t)
  (with-eval-after-load 'eglot
    (add-to-list 'eglot-server-programs
               `((rust-ts-mode rust-mode) . ("rust-analyzer" :initializationOptions
                                      (:cachePriming (:enable :json-false)))))))

;;;; Nix
(use-package nix-mode
  :mode "\\.nix\\'"
  :config
  (add-to-list 'format-all-formatters '("Nix" alejandra))
  (add-hook 'nix-mode-hook 'format-all-mode))

;;;; Terraform
(use-package terraform-mode
  :mode "\\.tf\\'"
  :config
  (add-to-list 'format-all-formatters '("Terraform" terraform-fmt))
  (add-hook 'terraform-mode-hook 'format-all-mode))

;;;; Go
(use-package go-mode
  :mode "\\.go"
  :config
  (add-to-list 'format-all-formatters '("Go" gofmt))
  (add-hook 'go-mode-hook 'format-all-mode))

;;;; Agda
(if (executable-find "agda-mode")
    (progn (load-file (let ((coding-system-for-read 'utf-8))
                        (shell-command-to-string "agda-mode locate")))

           (setq auto-mode-alist
                 (append
                  '(("\\.agda\\'" . agda2-mode)
                    ("\\.lagda.md\\'" . agda2-mode))
                  auto-mode-alist))))
(defun set-agda-input-method ()
  (evil-without-input-method-hooks ;; Disable evil's hooks which reset current-input-method in favor of evil-input-method before the evil minor mode has been loaded.
    (set-input-method "Agda")))
(add-hook 'agda2-mode-hook 'set-agda-input-method)

;;;; Haskell
(use-package haskell-mode)

;;;; Python
(add-to-list 'format-all-formatters '("Python" black))
(add-hook 'python-mode-hook 'format-all-mode)
(setq python-indent-offset 4)

;;;; YAML
(use-package yaml-mode
  :mode "\\.ya?ml\\'")

;;;; HTML + CSS + JS
(use-package web-mode
  :config
  (rassq-delete-all 'mhtml-mode auto-mode-alist)
  (rassq-delete-all 'html-mode auto-mode-alist)
  (setq web-mode-markup-indent-offset 4
        web-mode-enable-front-matter-block t
        web-mode-enable-auto-pairing t
        web-mode-enable-auto-quoting nil
        web-mode-engines-alist '(("django"    . "\\.njk\\'"))
        sgml-quick-keys 'close)
  :bind (:map web-mode-map
              ("TAB" . 'emmet-expand-line))
  :mode ("\\.html?\\'" "\\.html.tera\\'" "\\.njk\\'" "\\.astro\\'"))

(use-package emmet-mode
  :hook (web-mode . emmet-mode))

;;;; Markdown
(use-package markdown-mode)

;;;; LaTeX
(add-hook 'latex-mode-hook (lambda ()
                             (interactive)
                             (visual-fill-column-mode 1)
                             (display-line-numbers-mode 0)))
(add-to-list 'auto-mode-alist '("\\.tex\\'" . latex-mode))

;;;; Typst
(require 'treesit)
(add-to-list 'treesit-language-source-alist
             '(typst "https://github.com/uben0/tree-sitter-typst"))
(treesit-install-language-grammar 'typst)
(push (concat config-emacs-dir "typst-ts-mode") load-path)
(require 'typst-ts-mode)
(setq typst-ts-mode-watch-options "--open"
      typst-ts-mode-enable-raw-blocks-highlight t
      typst-ts-mode-highlight-raw-blocks-at-startup t)

(provide 'config-langs)
