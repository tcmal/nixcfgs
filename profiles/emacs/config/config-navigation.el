;;;; Keybinds
(use-package emacs
  :bind (:map leader-map
              ;; Quitting
              ("q q" . 'save-buffers-kill-terminal)
              ("q k" . 'save-buffers-kill-emacs)

              ;; Buffer navigation
              ("f" .  'find-file)
              ("j" .  'evil-switch-to-windows-last-buffer)
              ("w" .  'evil-window-map)
              ("m" .  'switch-to-next-buffer)
              ("n" .  'switch-to-prev-buffer)))

;;;; More navigation options
(use-package consult
  :defer nil
  :config
  (setq completion-in-region-function
        (lambda (&rest args)
          (apply #'consult-completion-in-region args))
        xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)
  :bind (:map leader-map
              ("e" . 'consult-flymake)
              ("s" . 'consult-line)
              ("d" . 'consult-imenu)
              ("b" . 'consult-buffer)))

;;;; project.el
(setq tab-bar-show nil)
(tab-bar-mode 1)
(use-package project
  :defer nil
  :config
  (require 'project)
  (setq doom-modeline-project-detection 'project
        doom-modeline-workspace-name t
        project-switch-commands 'project-find-file
        project-vc-extra-root-markers '(".project.el"))
  (customize-set-variable 'project-list-file (expand-file-name "projects.el" config-cache-dir))
  :bind (:map leader-map
              ("SPC" . project-find-file)
              ("p" . project-switch-project)
              ("TAB h" . tab-previous)
              ("TAB l" . tab-next)
              ("TAB k" . (lambda () (interactive) (project-kill-buffers t)))
              ("TAB r" . tab-rename)
              ("TAB TAB" . tab-switch)))

(use-package project-tab-groups
  :ensure
  :config
  (project-tab-groups-mode 1))

(provide 'config-navigation)
