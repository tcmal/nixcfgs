#!/usr/bin/env zx

const readline = require('readline');
const Writable = require('stream').Writable;

const BACKUPS_DEST = {
    'storage': {'emails': 'sftp:storage:backups/Emails', 'other': 'sftp:storage:backups/Other', 'sensitive': 'sftp:storage:backups/Sensitive'},
};
const BACKUPS_SRC = {
    emails: 'Emails/',
    other: 'Other/',
    sensitive: 'Sensitive/',
};
const RESTIC_TASKS = Object.keys(BACKUPS_SRC);
const RESTIC_DESTS = Object.keys(BACKUPS_DEST);

function questionPassword(prompt) {
    return new Promise((resolve, _) => {
        let muted = new Writable({
            write: function(chunk, encoding, callback) {
                callback();
            }
        });
        let rl = readline.createInterface({
            input: process.stdin,
            output: muted,
            terminal: true
        });

        process.stdout.write(chalk.bold(prompt));
        rl.question('', function(val) {
            rl.close();
            process.stdout.write('\n');
            resolve(val);
        });
    });
}

function argsOrAll(all, argName, shortArgName) {
    let sel = all;
    let arg = argv[argName] || argv[shortArgName];
    if (!!arg && typeof arg == 'string') {
        sel = arg.split(",");
        if (sel.some((x) => !all.includes(x))) {
            echo(chalk.red(`Invalid ${argName} selection. Valid tasks: ${all}`))
            process.exit(1);
        }
    }
    return sel;
}

async function doRestic(tasks, dests, creds) {
    process.env.RESTIC_PASSWORD = creds.restic_password;
    for (let dest of dests) {
        for (let task of tasks) {
            if (!RESTIC_TASKS.includes(task))
                continue;
            let repo = BACKUPS_DEST[dest][task];
            let src = BACKUPS_SRC[task];

            await $`restic backup -r ${repo} ${src}`;
        }
    }
}

const info = x => echo(chalk.blue(x));

async function doEmails() {
    await $`mbsync -a`;
    await $`notmuch new`;
}

cd(`${process.env.HOME}/Documents`);

let tasks = argsOrAll(RESTIC_TASKS, 'tasks', 't');
let restic_dests = argsOrAll(RESTIC_DESTS, 'restic_dests', 'd');
if (!RESTIC_TASKS.some(x => tasks.includes(x))) {
    restic_dests = [];
}

let creds = {};
if (restic_dests.length > 0) {
    creds.restic_password = await questionPassword('Restic Password => ');
}

echo``;
if (tasks.includes("emails"))
    await doEmails();

await doRestic(tasks, restic_dests, creds);

echo(chalk.green.bold("\nSuccess!"))
