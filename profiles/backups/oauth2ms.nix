{pkgs, ...}:
pkgs.python3Packages.buildPythonPackage {
  pname = "oauth2ms";
  version = "1.0.0";
  src = pkgs.fetchgit {
    url = "https://github.com/harishkrupo/oauth2ms.git";
    rev = "a1ef0cabfdea57e9309095954b90134604e21c08";
    sha256 = "sha256-xPSWlHJAXhhj5I6UMjUtH1EZqCZWHJMFWTu3a4k1ETc=";
  };

  patchPhase = ''
    cat > setup.py <<EOF
    from setuptools import setup, find_packages

    setup(name='oauth2ms',
        version='1.0',
        packages=find_packages(),
        scripts=["oauth2ms"],
          )
    EOF
  '';
  doCheck = false;

  propagatedBuildInputs = [
    pkgs.python3Packages.python-gnupg
    pkgs.python3Packages.pyxdg
    pkgs.python3Packages.msal
  ];
}
