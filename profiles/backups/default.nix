{
  pkgs,
  hmUser,
  ...
}: {
  home-manager.users.${hmUser} = {
    services.gnome-keyring = {
      enable = true;
      components = ["secrets"];
    };

    home.packages = with pkgs; [
      # Email archival
      isync
      (pkgs.callPackage ./oauth2ms.nix {})
      libsecret
      notmuch

      # Off-site backups
      nodePackages.zx
      gcr
      restic
      (let
        name = "backup-tasks";
        mjs = pkgs.writeTextFile {
          name = "${name}.mjs";
          executable = true;
          destination = "/bin/${name}.mjs";
          text = builtins.readFile ./backup.js;
        };
      in
        pkgs.writeScriptBin "backup-tasks" "#!${mjs}/bin/${name}.mjs")
    ];

    home.file.".mbsyncrc".source = ./mbsyncrc;
  };
}
