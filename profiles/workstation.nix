{...}: {
  imports = [
    ./emacs
    ./ssh.nix
    ./git.nix
    ./yubikey.nix
    ./firefox
    ./thunderbird.nix
    ./shell.nix
    ./files.nix
  ];
}
