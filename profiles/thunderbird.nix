{
  hmUser,
  lib,
  ...
}: {
  home-manager.users.${hmUser} = {
    programs.thunderbird = {
      enable = true;
      profiles."default" = {
        isDefault = true;
        settings = {
          # send plaintext
          "mail.default_send_format" = 1;

          "privacy.donottrackheader.enabled" = true;
        };
      };
    };

    accounts.email.accounts = let
      mkAccount = _: acct:
        lib.recursiveUpdate
        {
          thunderbird = {
            enable = true;
            perIdentitySettings = id: {
              "mail.identity.id_${id}.compose_html" = false;
              "mail.identity.id_${id}.reply_on_top" = 1;
            };
          };
        }
        acct;
      accts = {
        personal = {
          address = "me@aria.rip";
          userName = "me@aria.rip";
          realName = "Aria";
          primary = true;

          imap = {
            host = "imap.mailbox.org";
            port = 993;
          };
          smtp = {
            host = "smtp.mailbox.org";
            port = 465;
          };
        };
        student = rec {
          address = "s2080441@ed.ac.uk";
          userName = address;
          realName = "Aria Shrimpton";
          flavor = "outlook.office365.com";
          thunderbird.settings = id: {
            # use oauth
            "mail.server.server_${id}.authMethod" = 10;
            "mail.smtpserver.smtp_${id}.authMethod" = 10;
          };
        };
      };
    in
      builtins.mapAttrs mkAccount accts;
  };

  # Disable speech synthesis, as it's a huge dependency and not super useful to me
  nixpkgs.overlays = [
    (final: prev:
      prev
      // {
        thunderbird = prev.thunderbird.override {
          cfg.speechSynthesisSupport = false;
        };
      })
  ];
}
