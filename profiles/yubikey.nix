{
  pkgs,
  hmUser,
  ...
}: {
  environment.systemPackages = with pkgs; [yubikey-manager age age-plugin-yubikey];

  services.pcscd.enable = true;
  security.pam = {
    services.sudo.u2fAuth = true;
    u2f = {
      cue = true;
      authFile = "/etc/u2f-mappings";
    };
  };
}
